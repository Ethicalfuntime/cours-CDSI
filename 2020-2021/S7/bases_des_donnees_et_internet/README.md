# Info 
Le vrais cours d'HTML et CSS est un peu vieux voici une bonne alternative :
<https://www.youtube.com/watch?v=Y80juYcu3ZI&list=PLwLsbqvBlImHG5yeUCXJ1aqNMgUKi1NK3>

# Correction de TD

```sql
Question 5 du TD précédent;

5.      Donner, pour chaque série, son identifiant et le total des coefficients de ses épreuves 

select  IdE, sum(Coef) from EPREUVE 
group by IdE ;

6.      Donner l'identifiant de la série ayant le plus de candidats inscrits

select IdS from ELEVE
group by IdS 
having count(*) = (select max(count(*)) from ELEVE group by IdS);
                                   
7.      Donner pour chaque élève qui n'a pas passé un ou plusieurs examens de sa série son numéro et l'identifiant des matières qu'il n'a pas passées

select ELEVE.Num, EPREUVE.IdE
from ELEVE, EPREUVE
where ELEVE.IdS = EPREUVE.IdS
minus/except  // l’un des deux selon les SGBD
Select PASSER.Num, PASSER.IdE
from PASSER ;

8.      Donner pour chaque épreuve son identifiant et sa moyenne 

select IdE, avg(Note)
from PASSER
group by IdE ;
                                   
9.      Donner la désignation des séries comportant une épreuve de philosophie

select Désignation 
from SERIE, EPREUVE
where SERIE.IdS=EPREUVE.IdS
   and Matière = 'philosophie' ;
   
 
10.      Donner l'identifiant et la désignation des séries durant le plus longtemps  

select IdS, Désignation from SERIE
where IdS in ( select IdS from EPREUVE 
                        group by  IdS
                        having sum(Durée) = (  select max(sum(Durée)) from EPREUVE 
                                                           group by  IdS)) ;

11.      Indiquer les index positionnés automatiquement par le système, implanter les index qui vous semblent pertinents et indiquer les attributs qui ne doivent pas être indexés. Justifier vos réponses
Index positionnés automatiquement
·       Pour Elève : Num
·       Pour Série : IdS, Désignation
·       Pour Epreuve : IdE
·       Pour Passer : (Num, IDE)
Index positionnés manuellement
·       Pour Elève : create index IDX_ELEVE_IDS on ELEVE(IDS) ;
·       Pour Série : -
·       Pour Epreuve : create index IDX_EPREUVE_IDS on EPREUVE(IDS) ;
·       Pour Passer : create index IDX_PASSER_IDE on PASSER(IDE) ;
On positionne les index sur les clefs étrangères et sur les attributs les plus souvent mis à jours.  
```


```sql
1.      Implanter le schéma ci-dessus en utilisant le langage SQL. Vous proposerez les types les mieux adaptés pour chaque attribut sachant que les identifiants seront constitués de quatre caractères alphanumériques
create table COMMERCIAL (  IDCo char(4),
                                               NOMPRENOM varchar2(50),
                                               TELEPHONE   char(10)) ;
 
create table PRODUIT (IDP   char(4),
                                               LIBELLE varchar2(100),
                                               PU        number(4,2)) ;
 
create table CLIENT ( IDCl                 char(4),
                                               NOM               varchar2(50),
                                               ADRESSE        varchar2(100),
                                               IDCo               char(4) );
 
create table VENDRE (           IDCo               char(4),
                                               IDP                 char(4),
                                               IDCl                char(4),
                                               DATEV            Date,
                                               QTE                number);
```

```sql
1.      Implanter le schéma ci-dessus en utilisant le langage SQL. Vous proposerez les types les mieux adaptés pour chaque attribut sachant que les identifiants seront constitués de 3 caractères alphanumériques
create table CLIENT values (  IDC char(3),
NOMPRENOM varchar2(50),
AGE                 number,
SALAIRE        number(7, 2));
 
create table AGENCE values ( IDA char(3),
NOMPRENOM varchar2(50),
ADRESSE        varchar2(50),
TELEPHONE   char(10));

 
create table BIEN values (  IDB char(3),
ADRESSE        varchar2(50),
CODEPOSTAL char(5),
TYPE               char(3),
LOYER             number(6,2),
IDC                 char(3),
IDA                 char(3) );
 
create table INSCRIT  values(IDC  char(3), IDA  char(3));

2.      Implanter les contraintes de clés primaires et de clés étrangères

alter table CLIENT
add constraint CP_CLIENT primary key(IDC);
 
alter table AGENCE
add constraint CP_AGENCE primary key(IDA);
 
alter table BIEN
add constraint CP_BIEN primary key(IDB);
 
alter table INSCRIT
add constraint CP_INSCRIT primary key(IDC, IDA);
 
alter table BIEN
add (    constraint CE_BIEN_IDC foreign key (IDC) references CLIENT,
            constraint CE_BIEN_IDA foreign key (IDA) references AGENCE);
 
alter table INSCRIT
add (    constraint CE_INSCRIT_IDC foreign key (IDC) references CLIENT,
            constraint CE_INSCRIT_IDA foreign key (IDA) references AGENCE);
            
3. Implanter les contraintes sécurisant les données ( colonnes ) les plus sensibles      

a- dans la table client la colonnes NomPrenom , Salaire doivent être non NULL , la colonne Salaire doit être >= à montant de salaire ( par exemple 25000 € par an, etc.
( NomPrenon not null, Check ( Salaire >=25000),...

Dans la table Bien, Le Type doit être non null et reprendre sa valeur dans l'ensemble T1,T2,T3,... contrainte check Type IN ('T1,'T2','T3','T4') par exemple.
le Loyer dans être dans une fourchette Min et max.
Dans la table Agence les  tous les attributs sont non NULL.
4.      Donner l'adresse de tous les biens non encore loués

select ADRESSE from BIEN
where IDC is null;

5.      Donner l'identifiant des clients n'ayant toujours rien loué. Vous utiliserez l'opérateur ensembliste "minus" puis l'opérateur "in"

select IDC from CLIENT
minus/except (selon les SGBD)
select IDC from BIEN;
 
select IDC from CLIENT
where IDC not in (select IDC from BIEN where IDC is not null);

6.      Donner le nombre de biens non encore loués
select count(*) from BIEN
where IDC is null;
 
7.     Donner le nombre des biens loués par chaque agence. On donnera ce nombre et le code de l'agence

select IDA, count(*) from BIEN
where IDA is not null
group by IDA;

8.      Donner le salaire moyen des inscrits de l'agence d'identifiant "A12"
select avg(SALAIRE) from CLIENT, INSCRIT
where CLIENT.IDC=INSCRIT.IDC
and INSCRIT.IDA='A12';
 
9.     Donner le montant total des loyers perçus chaque mois par chaque agence. On considérera que les agences perçoivent tous les loyers des biens qu'elles louent. On donnera ce total et le code de l'agence
select IDA, sum(LOYER) from BIEN
where IDA is not null
group by IDA;

10.      Donner le nom de ou des agences qui perçoivent le moins de loyer

select NOM from AGENCE
where IDA in ( select IDA from BIEN
                        where  IDA is not null
                        group by IDA 
                        having sum(LOYER) = ( select min(sum(LOYER)) from BIEN
                                                           where IDA is not null
                                                           group by IDA));

TP3 3)
alter table ESTCOMPOSEE modify RANG constraint NN_ESTCOMPOSEE_RANG not null ;
alter table ESTCOMPOSEE add constraint UNIQ_ESTCOMPOSEE_RANG_NUM unique (RANG, NUM) ;
```


```sql
TD 3 partie 2 :
Au Niveau du PHP vous devez reconfigurer votre php.ini
PHP

Afin de parfaire la sécurité de PHP nous veillons à définir les paramètres suivants dans le fichier php.ini

# Désactiver l'insersion de la version de PHP # Dans les headers HTTP expose_php = Off

# Limiter l'utilisation de la mémoire par script Memory limit = 128M

# Supprimer l'affichage des erreurs display_errors = Off

# Désactiver l'upload de fichiers file_uploads = Off

# Empêcher l'ouverture d'url et leur inclusion # Local / Remote File inclusion allow_url_fopen = Off allow_url_include = Off

# Activer l'extension MySQLi (pour plus tard) extension=mysqli

# utiliser les secure cookies via https session.cookie_secure = On

# Les cookies ne seront pas accessibles par des # moteurs de scripting comme JavaScript session.cookie_httponly = On # Forcer PHP à récupérer un cookie pour maintenir # la session en vue de se protéger d'attaques # "hijacking" session.use_only_cookies = On # Faire en sorte que le cookie expire quand le # navigateur redémarre session.cookie_lifetime = 0 # Longueur du cookie session.sid_length = 64 # Fixer les sessions session.use_strict_mode = 1 # Restreindre l'accès aux dossiers open_basedir = /var/www/html/:/var/www/html2/ # Désactiver les fonctions permettant l'exécution # de commandes système.

disable_functions = pcntl_alarm,pcntl_fork,pcntl_waitpid,pcntl_wait,pcntl_wifexited, pcntl_wifstopped,pcntl_wifsignaled,pcntl_wifcontinued, pcntl_wexitstatus,pcntl_wtermsig,pcntl_wstopsig,pcntl_signal, pcntl_signal_dispatch,pcntl_get_last_error,pcntl_strerror, pcntl_sigprocmask,pcntl_sigwaitinfo,pcntl_sigtimedwait,pcntl_exec, pcntl_getpriority,pcntl_setpriority,exec,shell_exec,passthru,system 
```
```sql
Au niveau de Mysql


Mysql

sudo mysql_secure_installation

Répondre:

Remove anonymous users = yes Disallow root login remotely = yes Remove test database and access to it = yes Reload privilege tables now = yes

CREATE DATABASE `secure_login`; CREATE USER 'dba'@'localhost' IDENTIFIED BY 'S3cUr3p@$sw0rD!'; GRANT SELECT, INSERT, UPDATE ON `secure_login`.* TO 'dba'@'localhost'; CREATE TABLE `secure_login`.`members` (

`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,

`username` VARCHAR(30) NOT NULL,

`email` VARCHAR(50) NOT NULL,

`password` CHAR(128) NOT NULL ) ENGINE = InnoDB;

CREATE TABLE `secure_login`.`login_attempts` ( `user_id` INT(11) NOT NULL, `time` VARCHAR(30) NOT NULL ) ENGINE=InnoDB;

INSERT INTO `secure_login`.`members` VALUES(1, 'test_user', 'test@example.com', '$2y$10$IrzYJi10j3Jy/K6jzSLQtOLif1wEZqTRQoK3DcS3jdnFEhL4fWM4G'); 
```

```sql
TD 3 partie 1 : 
   1.      Implanter le schéma de la base de données sans les contraintes

Create table LIGNE ( num      NUMBER,
                                    Duree  NUMBER(2)) ;
 
Create table STATION (        ids       CHAR(3),
                                               nom      VARCHAR2(20)) ;
 
create table ESTCOMPOSEE( num      NUMBER,
                                               ids       CHAR(3),
                                               rang     NUMBER(2)) ;
 
2.     Implanter les contraintes de clefs primaires et de clefs étrangères
 
alter table LIGNE add constraint CP_LIGNE primary key (num) ;
alter table STATION add constraint CP_STATION primary key (ids) ;
alter table ESTCOMPOSEE add constraint CP_ESTCOMPOSEE primary key (num, ids) ;
 
alter table ESTCOMPOSEE
add (    constraint CE_EST_COMPOSEE_NUM foreign key (NUM) references LIGNE,
            constraint CE_EST_COMPOSEE_IDS foreign key (IDS) references STATION) ;
 
3.     Implanter les contraintes qui expriment que le couple (NUM, RANG) est une clef candidate
alter table ESTCOMPOSEE modify RANG constraint NN_ESTCOMPOSEE_RANG not null ;
alter table ESTCOMPOSEE add constraint UNIQ_ESTCOMPOSEE_RANG_NUM unique (RANG, NUM) ;
 
4.     Indiquer les index positionnés automatiquement par le système, implanter les index qui vous semblent pertinents et indiquer les attributs qui ne doivent pas être indexés. Justifier votre réponse
Index implicites :
NUM         dans LIGNE        à clef primaire
IDS         dans STATION     à clef primaire
NUM, IDS       dans ESTCOMPOSEE à clef primaire
RANG, NUM    dans ESTCOMPOSEE  à contrainte unique
 
Index à positionner par l’utilisateur
            NOM               dans STATION           à C’est un attribut qui semble être très utilisé
            IDS                 dans ESTCOMPOSEE  à C’est une partie gauche de clef primaire
 
Attributs à ne surtout pas indexer  
Ce sont les attributs dont le contenu est dynamique tel que Durée, ...
```
```
5.  Indiquer les attributs ou groupes d’attributs qui sont « not null » et ceux qui sont uniques. Justifier votre réponse
Attribut « not null » :
    
NUM                dans LIGNE                 à clef primaire
IDS                 dans STATION           à clef primaire
NUM, IDS       dans ESTCOMPOSEE à partie de clef primaire
IDS                 dans ESTCOMPOSEE à partie de clef primaire
RANG              dans ESTCOMPOSEE  à contrainte positionnée par l’utilisateur
NOM               dans STATION           à contrainte devant être positionnée
 
Attribut(s) unique(s) :
NUM                dans LIGNE                 à clef primaire
IDS                 dans STATION           à clef primaire
NUM, IDS       dans ESTCOMPOSEE à clef primaire
RANG, NUM    dans ESTCOMPOSEE  à contrainte unique
            
6.      Implanter les contraintes qui expriment que la durée de parcours d’une ligne de bout en bout est comprise entre 10 et 60 minutes, et que le rang est toujours strictement positif.

alter table LIGNE add constraint CHK_LIGNE_DUREE check DUREE >=10 and DUREE <=60 ;
alter table ESTCOMPOSEE add constraint CHK_ESTCOMPOSEE check RANG>0 ;

7.      Donner le nom des stations qui offrent des correspondances. Une station qui offre une correspondance est une station commune à plusieurs lignes

select NOM from STATION
where IDS in ( select IDS from ESTCOMPOSEE
                        group by IDS having count(*) > 1) ;
```
```sql
8.      Donner le numéro des lignes passant par les stations « Bastille » et « Reuilly-Diderot »


select NUM from ESTCOMPOSEE, STATION
where ESTCOMPOSEE.IDS=STATION.IDS
and       NOM="Reuilly-Diderot"
intersect
select NUM from ESTCOMPOSEE, STATION
where ESTCOMPOSEE. IDS=STATION. IDS
and       NOM="Bastille" ;
 
 
 OU 
 
select NUM from ESTCOMPOSEE, STATION
where ESTCOMPOSEE.IDS=STATION.IDS
and       NOM="Reuilly-Diderot"
and       NUM in (          select NUM from ESTCOMPOSEE, STATION
                                    where ESTCOMPOSEE.IDS=STATION.IDS
                                    and      NOM="Bastille") ;

9.      Donner le numéro des lignes à emprunter pour aller de la station « Saint-Lazare » à la station « Nation »  en effectuant un et un seul changement de ligne

select  EC3.NUM, EC4.NUM
from    ESTCOMPOSEE EC1, ESTCOMPOSEE EC2, ESTCOMPOSEE EC3, ESTCOMPOSEE EC4,
            STATION ST1, STATION ST2
where EC1.IDS=EC2.IDS
and       EC1.NUM=EC3.NUM
and       EC2.NUM=EC4.NUM
and       EC3.IDS=ST1.IDS
and       EC4.IDS=ST2.IDS
and       ST1.NOM="Saint-Lazare"
and       ST2.NOM="Nation"
 
10.     Donner pour chaque ligne le nombre de stations qu’elle traverse

select NUM, count(*) from ESTCOMPOSEE
group by NUM ;
```







